import COAPKerbDomain.AuthenticationServer.ASResponse;
import COAPKerbDomain.AuthenticationServer.ClientCredentials;
import COAPKerbDomain.AuthenticationServer.ClientTGTRequest;
import COAPKerbDomain.GenericResourceData;
import COAPKerbDomain.KerbEnvelope;
import COAPKerbDomain.Resource;
import COAPKerbDomain.TicketGrantingServer.TGSRequest;
import COAPKerbDomain.TicketGrantingServer.TGSRequestContent;
import COAPKerbDomain.TicketGrantingServer.TGSResponse;
import COAPKerbDomain.TicketGrantingServer.TGSResponseContent;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

public class NFCClient {

    private static String AuthServerURI = "coap://192.168.1.121:5683/Authenticate";
    private static String TGSServerURI  = "coap://192.168.1.121:5683/TicketGrantingService";

    //HardCoded user credentials
    private static String USER_PASSWORD = "iotlab1@";
    private static String USER_NAME = "user1";
    private static String DUMMY_IP = "192.168.1.1";

    private static HashMap<String, String> userDB = new HashMap();

    public static void main(String args[]){
        URI authServerURI = null;
        URI tgsURI = null;

        setFakeResData();

        try {
            authServerURI = new URI(AuthServerURI);
            tgsURI = new URI(TGSServerURI);
        } catch (URISyntaxException e){
            System.err.println("Invalid URI: " + e.getMessage());
            System.exit(-1);
        }

        //Authentication and TGT request
        CoapClient client = new CoapClient(authServerURI);
        byte[] cborData = prepareRequest(USER_NAME,USER_PASSWORD, DUMMY_IP);
        CoapResponse response = client.post(cborData, MediaTypeRegistry.APPLICATION_CBOR);
        client.shutdown();

        //Now we have the TGT and can request the specific resource ticket
        if (response!=null) {
            System.out.println(response.getCode());
            System.out.println(response.getOptions());
            requestResourceTicket(response,tgsURI);
        } else {
            System.out.println("No response received.");
        }
    }

    private static void requestResourceTicket(CoapResponse response,URI tgsURI) {

        System.out.println(response.getResponseText());

        TGSRequestContent tgsReqContent = new TGSRequestContent();
        TGSRequest tgsReq = new TGSRequest();

        try {
            ASResponse authServerResponse = ASResponse.toObject(response.getPayload());
            ClientCredentials clientCred = authServerResponse.getClientCredential(USER_PASSWORD);

            tgsReqContent.resource="camera";
            tgsReqContent.encryptedCredential = clientCred.encryptedCredential;

            tgsReq.setContent(tgsReqContent,clientCred.tgsSessionKey);

            CoapClient client = new CoapClient(tgsURI);
            CoapResponse tgsCoAPResponse = client.post(tgsReq.toCBOR(),MediaTypeRegistry.APPLICATION_CBOR);

            TGSResponse tgsResponse = TGSResponse.toObject(tgsCoAPResponse.getPayload());
            TGSResponseContent tgsResponseContent = tgsResponse.getContent(clientCred.tgsSessionKey);

            Resource cameraDeviceInfo = tgsResponseContent.resourceInfo;

//            KerbEnvelope kerbEnvelope = new KerbEnvelope();
//            kerbEnvelope.encryptedCrendential = tgsResponseContent.encryptedCredential;
//
//            GenericResourceData genericResourceData = new GenericResourceData();
//            genericResourceData.data = "pranut";
//
//            kerbEnvelope.setContent(genericResourceData,tgsResponseContent.sessionKey);
//
//            CoapClient client2 = new CoapClient(cameraDeviceInfo.resourceKerberizedURI);
//            CoapResponse tgsCoAPResponse2 = client2.post(kerbEnvelope.toCBOR(), MediaTypeRegistry.APPLICATION_CBOR);
//
            NFCReader nfcReader = new NFCReader(userDB,cameraDeviceInfo,tgsResponseContent);
            nfcReader.run();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static byte[] prepareRequest(String userName,String userPassword,String userIP) {
        ClientTGTRequest utr = new ClientTGTRequest(userName);
        byte[] cborData = null;
        try {
            utr.setUserSignature(userIP,userPassword);
            cborData= utr.toCBOR();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cborData;
    }

    private static void setFakeResData(){

        userDB.put("c3a03e7c","henrique");
        userDB.put("8466236e","pranut");
        userDB.put("637e3d7c","zhengiang");
        userDB.put("705b7b28","veronica");

        userDB.put("76dc6322","jonathan");
        userDB.put("911013a3","mohammad");

    }

}
