import COAPKerbDomain.GenericResourceData;
import COAPKerbDomain.KerbEnvelope;
import COAPKerbDomain.Resource;
import COAPKerbDomain.TicketGrantingServer.TGSResponseContent;
import com.pi4j.io.gpio.*;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.StringTokenizer;

public class NFCReader implements Runnable {

    Resource res;
    URI cameraKURI = null;
    // create gpio controller instance
    final GpioController gpio = GpioFactory.getInstance();
    GpioPinDigitalOutput buzzer;

    TGSResponseContent tgsResponseContent;
    HashMap<String, String> userDB;

    public NFCReader(HashMap<String, String> userDB, Resource cameraDeviceInfo, TGSResponseContent tgsResponseContent) throws URISyntaxException {

        res = cameraDeviceInfo;
        cameraKURI = new URI(cameraDeviceInfo.resourceKerberizedURI);
        this.tgsResponseContent = tgsResponseContent;
        this.userDB = userDB;

        buzzer = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27,   // PIN NUMBER
                "My LED",           // PIN FRIENDLY NAME (optional)
                PinState.LOW);
    }

    public void run() {

        String uid[] = null;
        Process p;
        try {
            while(true) {
                System.out.println("Please approximate your PIT ID");

                p = Runtime.getRuntime().exec("nfc-poll");
                BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));

                String line;
                while ((line = br.readLine()) != null) {
                    line = line.trim();
                    StringTokenizer st = new StringTokenizer(line);
                    String s = "";
                    if (line.startsWith("UID")) {
                        uid = line.split("\\s");
                        for (int x = 2; x < uid.length; x++) {
                            s += uid[x];
                        }

                        KerbEnvelope kerbEnvelope = new KerbEnvelope();
                        kerbEnvelope.encryptedCrendential = tgsResponseContent.encryptedCredential;

                        GenericResourceData genericResourceData = new GenericResourceData();

                        if(userDB.containsKey(s)){
                            String userName = userDB.get(s);
                            System.out.println("User :" + userName + " was found!");
                            genericResourceData.data = userName;

                            kerbEnvelope.setContent(genericResourceData,tgsResponseContent.sessionKey);

                            CoapClient client = new CoapClient(cameraKURI);
                            CoapResponse tgsCoAPResponse = client.post(kerbEnvelope.toCBOR(), MediaTypeRegistry.APPLICATION_CBOR);

                            buzzer.high();
                            Thread.sleep(100);
                            buzzer.low();
                        }else{
                            buzzer.high();
                            Thread.sleep(100);
                            buzzer.low();
                            Thread.sleep(100);
                            buzzer.high();
                            Thread.sleep(100);
                            buzzer.low();
                        }
                    }
                }

                p.waitFor();
                System.out.println("exit: " + p.exitValue());
                p.destroy();
            }
        } catch (Exception e) {
            System.out.println (e);
        }
    }
}
